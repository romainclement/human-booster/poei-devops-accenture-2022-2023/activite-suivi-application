FROM python:3.10.9-slim

ENV HOST 0.0.0.0
ENV PORT 5000

RUN set -ex && pip install --upgrade pip && pip install pipenv

RUN mkdir -p /app
WORKDIR /app

COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

RUN set -ex && pipenv install --deploy --system

COPY . /app

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

EXPOSE ${PORT}

CMD ["gunicorn", "--access-logfile=-", "--error-logfile=-", "time_tracker.app:app"]