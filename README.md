![Patch](patch.png)

# Suivi de l’application déployée

> Gestion du cycle complet d'une application

## Jeu de rôle

Vous venez d'intégrer l'équipe DevOps ! En guise de démarrage, on vous confie le suivi d'une application interne déployée sur l'infrastructure Microsoft Azure. Cette application sert à suivre le temps passé journalier des collaborateurs et est déjà utilisée par toute l'entreprise. Cette application a été développée par un stagiaire qui a malheureusement quitté l'entreprise.

Hélas, ce qui semblait être une tâche facile pour commencer votre nouveau poste démarre mal ! Plusieurs problèmes sont déjà remontés par les utilisateurs :

> Certaines actions sur l'application sont lentes : on aimerait ne pas perdre encore plus de temps à noter nos heures !
>
> _Martin A., dev guru_

> Quand j'affiche mes évènements enregistrés, ils ne sont pas triés par ordre chronologiques décroissants, c'est une galère pour s'y retrouver, je perds un temps monstrueux !
>
> _Jacqueline M., cheffe comptable_

> Je viens de m'apercevoir que les employés peuvent renseigner des évènements à 0 heures ! Et bien évidemment il semblerait que des petits malins en profite déjà ... Mais qu'est-ce que c'est que cette application ?! Allôôôôô !
>
> _Nicole B., directrice des ressources_

> Hier matin, j'ai vu Jean-Louis essayer plusieurs fois de se connecter avec le compte de Jean-Paul. Je voyais bien qu'au début il essayer de taper des mots de passe simples du genre "123456" mais au bout d'un moment il a taper un truc super complexe, un peu comme du code (?!) et là bim ! Il était connecté avec le compte de Jean-Paul, et il s'amusait à enregistrer des événements à 0 heures !
>
> _Tony J., assureur qualité_

Par ailleurs, l'équipe de White Hat interne nous a envoyé le message suivant :

> Il faudrait apprendre les rudiments de la sécurité pour le développement web ! Vous stockez la clé de signature des cookies de session en dur dans le code ! Réveillez-vous les n00bs avant que l'on vous pwned !
>
> _Mr Robot, l'homme à la capuche_

Vous avez 2 jours pour résoudre ces problèmes, mais vous n'êtes pas seuls ! Vous pouvez compter sur le support de l'équipe DevOps qui vous guidera dans votre quête.

Vous devrez documenter vos observations ainsi que les solutions mises en oeuvre. Bien entendu, toutes les améliorations devront être versionnées et respecter les processus déjà en place (Terraform, GitLab, etc).

## Test en local

Pour tester l'application en local avec Docker Compose :

```
docker compose up --build
```

Ouvrir l'application : http://localhost:5000

## Mise en place CI/CD

### Azure

Afin de provisionner l'infrastructure sur Microsoft Azure avec Terraform :

1. Créer un compte ou se connecter sur le [portail Azure](https://portal.azure.com)

2. Dans la section **Active Directory / App registrations** :
    - Créer une nouvelle application nommée "Terraform" (prendre note des identifiants "Tenant ID" et "Client ID")
    - Créer un nouveau secret client pour l'application dans la section "Certificates and secrets" (prendre note du secret "Secret Value")

3. Dans la section **Subscription / Access control (IAM)** :
    - Assigner le rôle "Owner" à l'application enregistrée

### GitLab

1. Forker (ou cloner puis pousser) ce dépôt GitLab dans votre espace personnel.

2. Dans la section **Settings / Repository / Deploy tokens**, créer un [token de déploiement](https://docs.gitlab.com/ee/user/project/deploy_tokens/#create-a-deploy-token) :
    - Nom : "Azure"
    - Permissions : `read_registry`
    - Prendre note du `username` et `password` !

3. Dans la section **Settings / CI/CD / Variables**, remplir les variables d'environnement suivantes en tant que masquées et protégées :
   - `ARM_SUBSCRIPTION_ID` : identifiant de souscription Azure
   - `ARM_TENANT_ID` : identifiant tenant de l'application Azure enregistrée
   - `ARM_CLIENT_ID` : identifiant client de l'application Azure enregistrée
   - `ARM_CLIENT_SECRET` : secret client de l'application Azure enregistrée
   - `DOCKER_REGISTRY_USERNAME` : le username du token de déploiement GitLab
   - `DOCKER_REGISTRY_PASSWORD` : le password du token de déploiement GitLab

4. Déclencher le déploiement de l'environnements depuis la branche `main` via GitLab-CI (section **CI/CD Pipelines / Run Pipeline**)
    - Etre patient, la provision initiale de l'infrastructure prend du temps
    - Dans les logs, prendre note des URLs des applications déployées dans chaque environnement (de la forme `https://timetracker-xxxxxxxx.azurewebsites.net`)

**Note importante** : penser à détruire l'infrastructure une fois l'activité terminée pour limiter les coûts (déclencher le job manuel "tf:destroy" dans GitLab-CI).
