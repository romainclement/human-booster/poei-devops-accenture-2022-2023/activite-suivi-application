terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.38.0"
    }
  }

  backend "http" {
  }
}

# ----------------------------------------------------------------------------

variable "DOCKER_REGISTRY_URL" {
  description = "Docker registry URL"
  type        = string
  sensitive   = false

  validation {
    condition     = length(var.DOCKER_REGISTRY_URL) > 0
    error_message = "Docker registry server URL is required"
  }
}

variable "DOCKER_REGISTRY_USERNAME" {
  description = "Docker registry username"
  type        = string
  sensitive   = false

  validation {
    condition     = length(var.DOCKER_REGISTRY_USERNAME) > 0
    error_message = "Docker registry server username is required"
  }
}

variable "DOCKER_REGISTRY_PASSWORD" {
  description = "Docker registry password"
  type        = string
  sensitive   = false

  validation {
    condition     = length(var.DOCKER_REGISTRY_PASSWORD) > 0
    error_message = "Docker registry server password is required"
  }
}

variable "DOCKER_IMAGE_NAME" {
  description = "Docker image name"
  type        = string
  sensitive   = false

  validation {
    condition     = length(var.DOCKER_IMAGE_NAME) > 0
    error_message = "Docker image name is required"
  }
}

variable "DOCKER_IMAGE_TAG" {
  description = "Docker image tag"
  type        = string
  sensitive   = false

  validation {
    condition     = length(var.DOCKER_IMAGE_TAG) > 0
    error_message = "Docker image tag is required"
  }
}

# ----------------------------------------------------------------------------

provider "azurerm" {
  features {}
}

data "azurerm_subscription" "default" {
}

resource "azurerm_resource_group" "default" {
  name     = "timetracker"
  location = "West Europe"
}

# ----------------------------------------------------------------------------

resource "random_string" "pg_id" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = true
}

resource "random_string" "pg_username" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = false
}

resource "random_password" "pg_password" {
  length  = 32
  special = false
}

resource "azurerm_postgresql_flexible_server" "default" {
  name                   = "timetrackerpostgres${random_string.pg_id.result}"
  location               = azurerm_resource_group.default.location
  resource_group_name    = azurerm_resource_group.default.name
  version                = "14"
  administrator_login    = random_string.pg_username.result
  administrator_password = random_password.pg_password.result
  storage_mb             = 32768
  sku_name               = "B_Standard_B1ms"
  zone                   = "1"
}

resource "azurerm_postgresql_flexible_server_database" "default" {
  name      = "timetracker-db"
  server_id = azurerm_postgresql_flexible_server.default.id
  collation = "en_US.utf8"
  charset   = "utf8"
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "default" {
  name             = "public-access"
  server_id        = azurerm_postgresql_flexible_server.default.id
  start_ip_address = "0.0.0.0"
  end_ip_address   = "255.255.255.255"
}

locals {
  pg_database_uri = "postgresql://${random_string.pg_username.result}:${random_password.pg_password.result}@${azurerm_postgresql_flexible_server.default.fqdn}:5432/${azurerm_postgresql_flexible_server_database.default.name}?sslmode=require"
}

# ----------------------------------------------------------------------------

resource "random_string" "app_id" {
  length  = 8
  special = false
  lower   = true
  upper   = false
  numeric = true
}

resource "azurerm_service_plan" "default" {
  name                = "app-service-plan"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  os_type             = "Linux"
  sku_name            = "F1"
}

resource "azurerm_linux_web_app" "default" {
  name                = "timetracker-${random_string.app_id.result}"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_service_plan.default.location
  service_plan_id     = azurerm_service_plan.default.id
  https_only          = true

  site_config {
    always_on          = false
    websockets_enabled = true

    application_stack {
      docker_image     = var.DOCKER_IMAGE_NAME
      docker_image_tag = var.DOCKER_IMAGE_TAG
    }
  }

  app_settings = {
    WEBSITES_PORT                   = "5000"
    DATABASE_URL                    = local.pg_database_uri
    DOCKER_REGISTRY_SERVER_URL      = var.DOCKER_REGISTRY_URL
    DOCKER_REGISTRY_SERVER_USERNAME = var.DOCKER_REGISTRY_USERNAME
    DOCKER_REGISTRY_SERVER_PASSWORD = var.DOCKER_REGISTRY_PASSWORD
  }
}

# ----------------------------------------------------------------------------

output "app_url" {
  description = "Application URL"
  value       = "https://${azurerm_linux_web_app.default.default_hostname}"
  sensitive   = false
}

output "database_uri" {
  description = "Database URI"
  value       = local.pg_database_uri
  sensitive   = true
}
